package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DirectoryContent {

    private static final Logger log = LogManager.getLogger();
    private final Scanner scanner =new Scanner(System.in);
    private static List<File> files = new ArrayList<>();

        public String getDirectoryPath(){
            System.out.println("Enter path of directory:");
            String path ="C:/Users/User/Desktop/Java-lab24/IO-NIO";
            //String path =scanner.nextLine();
            return path;
        }
      public void showDirectoryTree(){
            File file = new File(getDirectoryPath());

            List<File> myList = doListing(file);

            myList.forEach(System.out::println);
        }

        public static List<File> doListing(File dirName) {

            File[] fileList = dirName.listFiles();

            for (File file : fileList) {

                if (file.isFile()) {

                    files.add(file);
                } else if (file.isDirectory()) {

                    files.add(file);
                    doListing(file);
                }
            }

            return files;
        }
    }

