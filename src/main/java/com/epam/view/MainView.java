package com.epam.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class MainView {
    private static final Logger log = LogManager.getLogger();
    private Scanner scanner = new Scanner(System.in);
    private String menu ="1 - read and write file usually!\n" +
            "2 - read and write file with buffer reader!\n" +
            "3 - show all content of file directory!\n" +
            "4 - serialize file into file!\n" +
            "5 - read java source file!\n"+
            "6-QUIT!";

    public void execute() {
        int choose;
        do {
            System.out.println(menu);
            choose = scanner.nextInt();
            switch (choose) {
                case 1:
                    new JavaFileReaderView().runWithUsualReader();
                    break;
                case 2:
                    new JavaFileReaderView().runWithBufferReader();
                    break;
                case 3:
                    new DirectoryContentView().run();
                    break;
                case 4:
                    new SerializationView().run();
                    break;
            }
        } while (!(6 == choose));
    }
}
