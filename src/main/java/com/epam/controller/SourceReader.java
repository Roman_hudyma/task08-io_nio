package com.epam.controller;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;

public class SourceReader {
    public void run( ) {
        List<String> stringList = readFile("JavaSourceCode.java");
        printComments(stringList);
    }

    public List<String> readFile(String path) {
        List<String> strings = new LinkedList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
                new FileInputStream(new File(path)), StandardCharsets.UTF_8.name()))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                strings.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return strings;
    }

    public void printComments(List<String> strings) {
        boolean isComment = false;
        for (String line : strings) {
            if (line.contains("/*") || isComment) {
                isComment = true;
                System.out.println(line);
                if (line.contains("*/")) {
                    isComment = false;
                }
            } else if (line.contains("//")) {
                System.out.println(line.substring(line.indexOf("//")));
            }
        }
    }

}
