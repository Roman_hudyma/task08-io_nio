package com.epam.view;

import com.epam.controller.SerializationController;
import com.epam.model.TestSerial;

public class SerializationView {
    SerializationController controller =new SerializationController();
    TestSerial testSerial =new TestSerial("roman",18,true,true);
    public void run()
    {
        controller.serializationObject(testSerial,"Object.txt");
        controller.deserializationObject(testSerial,"Object.txt");

    }
}
