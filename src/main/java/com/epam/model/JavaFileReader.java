package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class JavaFileReader {
    private static final Logger log = LogManager.getLogger();
  public void readFileUsually(String fileToRead,String fileToWrite) throws IOException {
     log.info("Reading usually...");
        FileInputStream in = null;
        FileOutputStream out = null;

          try {
            in = new FileInputStream(fileToRead);
            out = new FileOutputStream(fileToWrite);
            int c;

            while ((c = in.read()) != -1) {
                out.write(c);
            }
        } finally {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
            log.info("Successfully red!!");
        }
    }
}
