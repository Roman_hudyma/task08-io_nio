package com.epam.model.nio;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Scanner;

public class Client {

    private SocketChannel client;
    private ByteBuffer buffer;
    Scanner scanner= new Scanner(System.in);

    public Client() {
        try {
            InetSocketAddress inetSocketAddress = new InetSocketAddress(InetAddress.getLocalHost(), 7777);
            client = SocketChannel.open(inetSocketAddress);
            buffer = ByteBuffer.allocate(256);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stop() throws IOException {
        client.close();
        buffer = null;
    }

    public String sendMessage() {

        String message =scanner.nextLine();
        buffer = ByteBuffer.wrap(message.getBytes());
        String responseString = null;
        try {
            client.write(buffer);
            buffer.clear();
            client.read(buffer);
            responseString = new String(buffer.array()).trim();
            System.out.println(responseString);
            buffer.clear();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return responseString;
    }

    public SocketChannel getClient() {
        return client;
    }

    public void setClient(SocketChannel client) {
        this.client = client;
    }

    public static void main(String[] args) {
         Client client = new Client();
        client.sendMessage();
        Client client2 = new Client();
        client2.sendMessage();

    }
}