package com.epam.controller;

import com.epam.model.TestSerial;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class SerializationController {
    private static final Logger log = LogManager.getLogger();
    public void serializationObject(Object object,String filename)
    {
        try {
            FileOutputStream fos = new FileOutputStream(filename);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(object);
        oos.flush();
        oos.close();
        log.info("Successfully serialized!!!");
        }
        catch (IOException e)
        {
            log.error(e);
        }

    }
    public void deserializationObject(Object object,String filename){
        try
        {

            FileInputStream fis = new FileInputStream(filename);
            ObjectInputStream ois = new ObjectInputStream(fis);

            object = (TestSerial)ois.readObject();

            ois.close();
            fis.close();

           log.info("Object has been deserialized ");
            System.out.println("name:" + ((TestSerial) object).getName());
            System.out.println("age:" + ((TestSerial) object).getAge());
            System.out.println("is house:" + ((TestSerial) object).isHouse());
        }

        catch(IOException ex)
        {
            log.error("IOException is caught");
        }

        catch(ClassNotFoundException ex)
        {
            log.error("ClassNotFoundException is caught");
        }

    }
}

