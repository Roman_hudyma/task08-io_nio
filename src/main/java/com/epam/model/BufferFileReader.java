package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class BufferFileReader {
    private static final Logger log = LogManager.getLogger();

    public void readFileBuffer(String fileToRead, String fileToWrite, int bufferSize) throws IOException {
        log.info("Reading with buffer reader...");
        bufferSize = bufferSize * 1024 * 1024;
        try (BufferedReader br = new BufferedReader(new FileReader(fileToRead), bufferSize)) {
            String s;
            while ((s = br.readLine()) != null) {

                try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileToWrite))) {
                    bw.write(s);
                } catch (IOException ex) {
                    log.error(ex);
                }
            }
        } catch (Exception e) {
            log.error(e);
        }
    }
}
