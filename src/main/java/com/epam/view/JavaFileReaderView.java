package com.epam.view;

import com.epam.model.BufferFileReader;
import com.epam.model.JavaFileReader;

import java.io.IOException;

public class JavaFileReaderView {
    private final JavaFileReader reader =new JavaFileReader();
    private final BufferFileReader fileReader =new BufferFileReader();
    public void runWithUsualReader(){
        try {
            reader.readFileUsually("TextToRead.txt","TextToWrite.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void runWithBufferReader(){
        try {
            fileReader.readFileBuffer("TextToRead.txt","TextToWrite.txt",1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
