package com.epam.model;

import java.io.Serializable;

public class TestSerial implements Serializable {
    private String name;
    private  int age;
    private transient boolean car;
    private boolean house;

    public TestSerial(String name, int age, boolean car, boolean house) {
        this.name = name;
        this.age = age;
        this.car = car;
        this.house = house;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isCar() {
        return car;
    }

    public void setCar(boolean car) {
        this.car = car;
    }

    public boolean isHouse() {
        return house;
    }

    public void setHouse(boolean house) {
        this.house = house;
    }

}
